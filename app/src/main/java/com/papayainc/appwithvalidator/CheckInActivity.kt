package com.papayainc.appwithvalidator

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.github.gcacace.signaturepad.views.SignaturePad
import com.papayainc.appwithvalidator.databinding.ActivityCheckInBinding
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

@SuppressLint("ByteOrderMark")
class CheckInActivity : Toolbar(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityCheckInBinding>(this, R.layout.activity_check_in)
    }

    companion object {
        val TAG = "PermissionDemo"
        private val REQUEST_PERMISSION = 2000
        private const val REQUEST_STORAGE = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Check In", 0, " ")

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)

        revisaPermiso()

        binding.signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() { //Toast.makeText(SignActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            override fun onSigned() {
                binding.buttonConfirmar.setEnabled(true)
            }

            override fun onClear() {
                binding.buttonConfirmar.setEnabled(false)
            }
        })

        binding.setClickListener {
            when (it!!.id) {
                binding.buttonConfirmar.id -> {
                    validator.toValidate()
                }
            }
        }
    }

    fun revisaPermiso() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_STORAGE
            )
            Log.i(TAG, "Pide permiso")
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Si dio permiso")
            } else {
                Log.i(TAG, "No dio permiso")
            }
        }
    }

    fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            val file = File(
                path, "/" + (Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".png"
            )
            path.mkdirs();

            val fOut = FileOutputStream(file)

            signature.compress(Bitmap.CompressFormat.PNG, 85, fOut)
            fOut.flush()
            fOut.close()
            result = true

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    override fun onValidationSuccess() {
        val signatureBitmap: Bitmap = binding.signaturePad.getTransparentSignatureBitmap()
        if (addJpgSignatureToGallery(signatureBitmap)) {
            Toast.makeText(this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show()
            onBackPressed()
        } else {
            Toast.makeText(
                this,
                "Unable to store the signature",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onValidationError() {
        Toast.makeText(this, "Datos Erróneos", Toast.LENGTH_SHORT).show()
    }
}
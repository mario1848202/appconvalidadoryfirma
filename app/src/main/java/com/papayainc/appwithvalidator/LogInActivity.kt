package com.papayainc.appwithvalidator

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.papayainc.appwithvalidator.databinding.ActivityLogInBinding

class LogInActivity : AppCompatActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityLogInBinding>(this, R.layout.activity_log_in)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.iniciar.id -> {
                    validator!!.toValidate()
                }
                binding.olvidePass.id -> {
                    intent = Intent(this, OlvidePassActivity::class.java)
                    startActivity(intent)
                }
                binding.registrar.id -> {
                    startActivity(Intent(this, RegActivity::class.java))
                }
            }
        }
    }

    override fun onValidationSuccess() {
//        Toast.makeText(this, "Todo ok ", Toast.LENGTH_LONG).show()
        intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}
package com.papayainc.appwithvalidator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.papayainc.appwithvalidator.databinding.ActivityMainBinding

class MainActivity : ToolbarIconButton() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inicializarToolbar(binding.barraP, this)
        binding.setClickListener {
            when (it!!.id) {
                binding.buttonPerfil.id -> {
                    startActivity(Intent(this, PerfilActivity::class.java))
                }
                binding.buttonCheckIn.id -> {
                    startActivity(Intent(this, CheckInActivity::class.java))
                }
            }
        }
    }
}
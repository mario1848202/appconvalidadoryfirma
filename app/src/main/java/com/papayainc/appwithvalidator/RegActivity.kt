package com.papayainc.appwithvalidator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.papayainc.appwithvalidator.databinding.ActivityRegBinding

class RegActivity : Toolbar(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegBinding>(this, R.layout.activity_reg)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Registro", 0, " ")
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.buttonRegistrar.id -> {
                    validator.toValidate()
                }
            }
        }
    }

    override fun onValidationSuccess() {
        intent = Intent(this, LogInActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onValidationError() {
        Toast.makeText(this, "Datos Erróneos", Toast.LENGTH_SHORT).show()
    }
}
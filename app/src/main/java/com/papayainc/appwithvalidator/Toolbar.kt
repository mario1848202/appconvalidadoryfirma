package com.papayainc.appwithvalidator

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.papayainc.appwithvalidator.databinding.ToolbarBinding

abstract class Toolbar : AppCompatActivity() {

    fun inicializarToolbar(
        toolbarBinding: ToolbarBinding,
        titulo: String = "",
        tipoBtn: Int,
        toastText: String
    ) {
        setSupportActionBar(toolbarBinding.toolbar)
        toolbarBinding.titulo = titulo
        if (tipoBtn == 1)
            toolbarBinding.ivBtn1.setImageResource(R.drawable.usuario_solid)

        toolbarBinding.setClickListener {
            when (it!!.id) {
                toolbarBinding.ivBtn1.id -> {
                    Toast.makeText(this, toastText, Toast.LENGTH_LONG).show()
                }
            }
        }
        toolbarBinding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

    }
}
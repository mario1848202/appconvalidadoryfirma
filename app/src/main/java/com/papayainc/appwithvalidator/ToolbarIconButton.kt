package com.papayainc.appwithvalidator

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.papayainc.appwithvalidator.databinding.IconButtonToolbarBinding

abstract class ToolbarIconButton : AppCompatActivity() {
    fun inicializarToolbar(
        iconButtonToolbarBinding: IconButtonToolbarBinding,
        context: Context
    ) {
        setSupportActionBar(iconButtonToolbarBinding.toolbar)

        iconButtonToolbarBinding.setClickListener {
            when (it!!.id) {
                iconButtonToolbarBinding.buttonCerrarSesion.id -> {
                    startActivity(Intent(context, LogInActivity::class.java))
                    finish()
                }
            }
        }
        iconButtonToolbarBinding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

    }
}